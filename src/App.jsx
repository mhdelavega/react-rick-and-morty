import React, { useReducer, useState, useEffect} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Navbar, Home, Characters, Favourites  } from './components';
import './App.scss';

export const FavouritesContext = React.createContext(null);

export const ThemeContext = React.createContext(null);


const INITIAL_THEME = {
  fontSize: '16px',
  backgroundColor: 'white',
  color: 'black',
};

const App= () => {

  const [favourites, setFavourites] = useState([]);
  const [theme, setTheme] = useState(INITIAL_THEME);

  const saveFavourite = (character) => {
    // console.log(character);
    return setFavourites(favourites => [...favourites, character]);
  }

  const changeTheme = (theme) => setTheme(theme);


  return (
    <Router>
      <FavouritesContext.Provider value={{ favourites, saveFavourite }}>
        <ThemeContext.Provider value={{ theme, changeTheme }}>

          <div className="app">
            {/* <Navbar user={user} saveUser={saveUser} /> */}
            <Navbar />        
            <Switch>
              <Route 
                path="/" 
                exact component={() => 
                <Home/>}               
              />
              <Route 
                path="/characters" 
                exact component={() => 
                <Characters/>}               
              />
              <Route 
                path="/favourites" 
                exact component={() => 
                <Favourites/>}               
              />


          </Switch>
          </div>
        </ThemeContext.Provider>
      </FavouritesContext.Provider>
    </Router>

  );
}

export default App;
