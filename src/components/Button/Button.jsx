import { useState, useContext } from 'react';
import { ThemeContext } from '../../App';

const darkTheme = {
    backgroundColor: '#5C5C5C',
    color: 'white',
};

const lightTheme = {
    backgroundColor: 'white',
    color: 'black',
};

const SpecialButton = (props) => {
    const theme = useContext(ThemeContext);
    const [isDarkTheme, setIsDarkTheme] = useState(theme.theme.color === 'white');
    
    return (
        <button onClick={() => theme.changeTheme(isDarkTheme ? lightTheme : darkTheme)}>
            Activar modo {isDarkTheme ? 'claro' : 'oscuro'}
        </button>
    );
};

export default SpecialButton;
