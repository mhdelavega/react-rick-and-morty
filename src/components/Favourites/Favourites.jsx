import { useState, useContext } from 'react';
import { FavouritesContext } from '../../App';

// import './Favourites.scss';

const Favourites = (props) => {
    const { character } = props;
    const favourite = useContext(FavouritesContext);


    return (
        <div className="character-card">
           FAVORITOS:
           {/* {favourite.favourites[0].name} */}
           {favourite.favourites.map((character) => {
                        return  <div key={character.id} className="character-card">
                                    <h4>Id: {character.id}</h4>
                                    <h4>Name: {character.name}</h4>
                                    <img src={character.image} alt={character.name} />
                                </div>
                    })}

        </div>
    );
};

export default Favourites;
