import { useReducer, useEffect, useContext } from "react";
import { FavouritesContext } from '../../App';
import { CharacterCard } from '../../components';
import './Characters.scss';


const INITIAL_STATE = {
    characterList: [],
    info: {},
    isLoading: false,
    error: null,
    page: 0, 
};


const SET_CHARACTERS = 'SET_CHARACTERS';
const SET_ERROR =  'SET_ERROR';
const SET_IS_LOADING = 'SET_IS_LOADING';

const reducer = (state, action) => {
    const {type, payload} = action;

    switch (type) {
        case SET_CHARACTERS: 
            return { 
                ...state, 
                characterList: payload.results, 
                info: payload.info, 
                page: payload.page,
                isLoading: false };
        case SET_ERROR:
            return { ...state, error: payload, isLoading: false };
        case SET_IS_LOADING:
            return { ...state, isLoading: payload };
        default:
            return state;
    };

}

const Characters = () => {

    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
    // const favourites = [];


    useEffect(() => {

        dispatch({
            type: SET_IS_LOADING,
            payload: true
        });
        getData("https://rickandmortyapi.com/api/character/", 1);

    }, []);

    const getData = (url, newPage) => {

        fetch(url)
            .then((res) => res.json())
            .then((res) => {
                console.log(res)
                dispatch({
                    type: SET_CHARACTERS,
                    payload: {results: res.results, info: res.info, page: newPage},
                });
            })
            .catch((error) => {
                dispatch({
                    type: SET_ERROR,
                    payload: error.message,
                });
            });            
    };

    const onNextPageClick = () => getData(state.info.next, state.page + 1);

    const onPrevPageClick = () => getData(state.info.prev, state.page -1);

    const renderCharactersOrError = () => {
        if (state.error) {
            return <h3>{state.error}</h3>
        } else {
            return (
                <>
                    <div>
                        {/* <button className="b-btn"
                            onClick={onPrevPageClick}
                            disabled={!state.info.prev}
                        > */}
                        <button className="b-btn b-btn--arrow"
                            onClick={onPrevPageClick}
                            disabled={!state.info.prev}
                        >
                            <span class="c-arrow c-arrow--left"></span>                        
                        </button>

                        {state.info.next && 
                        <button className="b-btn b-btn--arrow"
                            onClick={onNextPageClick}
                        >
                            <span class="c-arrow c-arrow--right"></span> 
                        </button>}
                    </div>

                                             
                    <h2> Página: {state.page}</h2>
                    <div className="c-gallery">
                        {state.characterList.map((character) => {
                            return(
                                    <CharacterCard 
                                            key={character.id} 
                                            character={character} 
                                    />
                            ) 
                        })}
                    </div>
                </>
            )
        };
    };

    return (
        <div>
            <h2>Listado de personajes</h2>
            { state.isLoading
                ? <h1>Cargando datos...</h1>
                 : renderCharactersOrError()
            }
            
        </div>
    );
}

export default Characters;
