import { useState, useContext } from 'react';
import { FavouritesContext } from '../../App';

import './CharacterCard.scss';

const CharacterCard = (props) => {
    
    const favourite = useContext(FavouritesContext);

    const { character, addNewFavourite } = props;

    const handleClick = (e) => {
        // addNewFavourite(character.name);
        e.preventDefault();
        console.log('click handled:'+ character.name);
        return favourite.saveFavourite(character);
    }
    return (
        <div key={character.id} className="c-character-card">
            <div className="c-character-card__image-wrap">
                <img className="c-character-card__image" src={character.image} alt={character.name} />
            </div>
            <h3 className="c-character-card__text">{character.name}</h3>
            <button className="b-btn b-btn--secondary b-btn--s" onClick={handleClick}>
                Añadir a favoritos
            </button>
        </div>
    )
};

export default CharacterCard;
