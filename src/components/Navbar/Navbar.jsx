import { useContext } from 'react';
import { Link } from 'react-router-dom';
import { ThemeContext } from '../../App';
import './Navbar.scss';

const Navbar = (props) => {

    const theme = useContext(ThemeContext);

    return (
        <nav className="nav" style={theme.theme}>
            <div>
                <Link to="/">
                    <h2>Home</h2>
                </Link>
            </div>
            <div>
                <Link to="/characters">
                    <h2>Characters</h2>
                </Link>
            </div>
            <div>
                <Link to="/favourites">
                    <h2>Favourites</h2>
                </Link>
            </div>


        </nav>
    )
};

export default Navbar;
